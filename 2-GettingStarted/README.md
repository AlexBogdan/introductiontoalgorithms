# Introduction to Algorithms
## Chapter 2 - Getting Started

The first chapter that involves coding.

### Insertion Sort

Sorting a sequence by choosing an element and inserting it in the right position.

#### Python3 version (implementation + benchmark)

```python
# Actual sorting algorithm
def insertion_sort(self):
    for j in range(1, len(self.A)):
        key = self.A[j]
        i = j - 1
        while i >= 0 and self.A[i] > key:
            self.A[i + 1] = self.A[i]
            i -= 1
        self.A[i + 1] = key
```

| Input size    | Best case     | Average case  | Worst case  |
|:------------- |:------------- |:------------- |:----- |
| 10            | 0.000000 | 0.000000 | 0.000000 |
| 100           | 0.000000 | 0.002001 | 0.005004 |
| 200           | 0.000000 | 0.008005 | 0.016011 |
| 500           | 0.000000 | 0.048036 | 0.107075 |
| 1000          | 0.000999 | 0.214152 | 0.338241 |
| 2000          | 0.002000 | 0.929662 | 1.641165 |
| 5000          | 0.004003 | 5.018561 | 9.565789 |
