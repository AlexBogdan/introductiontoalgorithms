from random import shuffle
import time


class InsertionSort:
    # The sequence that will be sorted
    A = []

    def __init__(self, A):
        self.A = A

    # Actual sorting algorithm
    def insertion_sort(self):
        for j in range(1, len(self.A)):
            key = self.A[j]
            i = j - 1
            while i >= 0 and self.A[i] > key:
                self.A[i + 1] = self.A[i]
                i -= 1
            self.A[i + 1] = key

    # Timing the algorithm on the current sequence
    def test(self, verbose):
        if verbose:
            print(self.A)

        start_t = time.time()
        self.insertion_sort()
        end_t = time.time()

        if verbose:
            print(self.A)
        print("Size %d -> %f" % (len(self.A), end_t - start_t))

    # Simple test for a small sequence
    def default_test(self):
        print("Starting the default test")
        self.A = list(range(1, 10))
        shuffle(self.A)
        self.test(True)

    # Getting the benchmark for stressing tests
    def default_benchmark(self):
        INPUT_SIZE = [10, 100, 200, 500, 1000, 2000, 5000]

        print("Testing best-case")
        for N in INPUT_SIZE:
            self.A = list(range(1, N + 1))
            self.test(False)

        print("\nTesting average-case")
        for N in INPUT_SIZE:
            self.A = list(range(1, N + 1))
            shuffle(self.A)
            self.test(False)

        print("\nTesting worst-case")
        for N in INPUT_SIZE:
            self.A = list(range(1, N + 1))
            self.A.reverse()
            self.test(False)


instance = InsertionSort([])
instance.default_benchmark()
